<?php
include('include.php');

define ("MAX_SIZE","25600");

if(isset($_POST['Submit'])) {
  $filename = preg_replace("#[^a-z0-9_.-]#i", "", strtolower (stripslashes(mysql_real_escape_string(htmlentities($_FILES['image']['name'])))));
  $name = mysql_real_escape_string(htmlentities($_POST['name']));
  $email = mysql_real_escape_string(htmlentities($_POST['email']));
  $subject = mysql_real_escape_string(htmlentities($_POST['subject']));
  $comment = mysql_real_escape_string(htmlentities($_POST['comment']));
  $parent = mysql_real_escape_string(htmlentities($_POST['parent']));
  $board = mysql_real_escape_string(htmlentities($_POST['board']));

  if(strlen($name)==0)
    $name="Луковица";
  $image=$filename;
  
  if($_POST['parent']==-1 && !$image) { die('You must upload an image to your thread!'); }

  if ($image) {
    if ($_FILES['image']['size'] > MAX_SIZE*1024 ) {
      die ("ОШИБКА: Слишком большой файл!");
    }
    
    if (!getimagesize($_FILES['image']['tmp_name'])) {
      die("Неизвестный тип файла...");
    }
    $size=filesize($_FILES['image']['tmp_name']);

    if ($size > MAX_SIZE*1024) {
	die('<h1>Слишком большое изображение!</h1>');
	$errors=1;
    }
  } else {
    if (strlen($comment)<1)
      die("ОШИБКА: поле сообщения пустое.");
  }

  mysql_query("INSERT INTO `".$db."`.`post` (`id`, `parent`, `board`, `subject`, `name`, `email`, `text`, `time`) VALUES (NULL, '".$parent."', '".$board."', '".$subject."', '".$name."', '".$email."', '".$comment."', CURRENT_TIMESTAMP);") or die(mysql_error());
  $post = mysql_insert_id();
  if ($image)  {
    $info = pathinfo($filename);
    $newname="images/full/".$post.".".$info['extension'];
    $info['filename'] = str_replace(".","",$info['filename']);

    if(strlen($info['filename'])<1)
      $info['filename']="untitled";
    $copied = $errors==0 && copy($_FILES['image']['tmp_name'], $newname);

    if (!$copied) {
      die('<h1>Copy unsuccessfull!</h1>');
    }

    if(isset($_POST['Submit']) && $errors==0) {

      if($_POST['parent']==-1) {
	createthumb($newname,"images/thumb/".$post.".".$info['extension'],250,250);
      } else {
	createthumb($newname,"images/thumb/".$post.".".$info['extension'],200,200);
      }
    }
    mysql_query("INSERT INTO `".$db."`.`upload` (`id`, `name`, `board`, `post`, `type`, `size`) VALUES (NULL, '".$info['filename']."','".$board."',".$post.",'".$info['extension']."',".$_FILES['image']['size'].");") or die(mysql_error());

    if($_POST['parent']==-1)
      $_POST['parent']=$post;
  } 
}
header('Location: '.$url.'thread.php?board='.$_POST['board'].'&thread='.$_POST['parent']);
?>
